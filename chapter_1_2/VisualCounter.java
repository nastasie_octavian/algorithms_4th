public class VisualCounter {
	private final int N;
	private final int max;
	private int n = 0;
	private int tally;

	public VisualCounter(int N, int max) {
		this.N = N;
		this.max = max;
	}

	public void increment() {
		if(N > n && tally < max) {
			n++;
			tally++;
			StdDraw.point((double)n/N, 0.5 + (double)tally/(2.0*max));
		}
	}

	public void decrement() {
		if(N > n && tally > -max) {
			n++;
			tally--;
			StdDraw.point((double)n/N, 0.5 + (double)tally/(2.0*max));
		}
	}

	public int tally() {
		return tally;
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int max = Integer.parseInt(args[1]);
		double p = Double.parseDouble(args[2]);
		StdDraw.setPenRadius(0.01);
		VisualCounter vc = new VisualCounter(N, max);
		for(int i = 0; i < N; i++)
			if(StdRandom.bernoulli(p))
				vc.increment();
			else
				vc.decrement();
	}
}
