import java.util.Arrays;

public class BinarySearch {

	public static int rank(int key, int[] a, Counter c) { 
		c.increment();
		return rank(key, a, 0, a.length - 1);
	}

	public static int rank(int key, int[] a, int lo, int hi) { 
		// Index of key in a[], if present, is not smaller than lo
		if (lo > hi) return -1;
		int mid = lo + (hi - lo) / 2;
		if(key < a[mid]) return rank(key, a, lo, mid - 1);
		else if (key > a[mid]) return rank(key, a, mid + 1, hi);
		else return mid;
	}

    public static void main(String[] args) {
        int[] whitelist = In.readInts(args[0]);

        Arrays.sort(whitelist);

		Counter c = new Counter("Keys");
        // read key; print if not in whitelist
        while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            if (rank(key, whitelist, c) == -1)
                StdOut.println(key);
        }

		StdOut.println(c);
    }
}
