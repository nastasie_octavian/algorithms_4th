public class e1 {
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		Point2D[] points = new Point2D[N];
		for(int i = 0; i < N; i++)
			points[i] = new Point2D(Math.random(), Math.random());

		Point2D a = points[0], b = points[1];

		for(int i = 0; i < N; i++)
			for(int j = i + 1; j < N; j++) {
				if(points[i].distanceTo(points[j]) < a.distanceTo(b)) {
					a = points[i];
					b = points[j];
				}
			}
		
		StdDraw.setPenColor(StdDraw.GRAY);
		StdDraw.setPenRadius(.01);
		a.draw();
		b.draw();
		StdDraw.setPenRadius(.001);
		StdDraw.setPenColor(StdDraw.BLUE);
		a.drawTo(b);
	}
}
