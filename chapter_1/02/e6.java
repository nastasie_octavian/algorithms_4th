public class e6 {
	public static void main(String[] args) {
		String s = args[0], t = args[1], aux = s;
		while(t.indexOf(aux) < 0 && aux.length() > 0)
			aux = aux.substring(0, aux.length() - 1);

		StdOut.println("The bad one: " +  t.equals(s.substring(aux.length(),s.length()) + aux));
		StdOut.println("The good one: " 
			+ (s.length() == t.length() && ((s+s).indexOf(t) != -1)));
	}
}
