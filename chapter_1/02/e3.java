public class e3 {
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		
		int min = Integer.parseInt(args[1]);
		int max = Integer.parseInt(args[2]);

		Interval2D[] intervals = new Interval2D[N];
		Point2D[][] points = new Point2D[N][];
		for(int i = 0; i < N; i++) {
				double x1 = StdRandom.uniform(min, max);
				double x2 = StdRandom.uniform(min, max);
				if(x2 < x1) {
					double temp = x2;
					x2 = x1;
					x1 = x2;
				}
				Interval1D a = new Interval1D(x1 / max, x2 / max);				

				double y1 = StdRandom.uniform(min, max);
				double y2 = StdRandom.uniform(min, max);
				if(y2 < y1) {
					double temp = x2;
					y2 = y1;
					y1 = y2;
				}
				Interval1D b = new Interval1D(y1 / max, y2 / max);

				intervals[i] = new Interval2D(a, b);
				points[i] = new Point2D[]{ new Point2D(x1 / max, y1 / max), 
					new Point2D(x2 / max, y2 / max) };
			}

		int inter = 0;
		for(int i = 0; i < N; i++)
			for(int j = i + 1; j < N; j++) {
				if(intervals[i].intersects(intervals[j]))
					inter++;
			}
				
		System.out.println("Intersection pair count: " + inter);		
		
		int cont = 0;
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				if(i != j && intervals[i].contains(points[j][0]) &&
					intervals[i].contains(points[j][1]))
						cont++;

		System.out.println("Intervals contained in a nother interval count: " + cont);		
		for(int i = 0; i < N; i++)
			intervals[i].draw();
	}
}
