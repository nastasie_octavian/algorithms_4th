public class Accumulator {
	private double m;
	private double s;
	private int N;

	public void addDataValue(double x) {
		N++;
		s = s + 1.0 * (N - 1) / N * (x - m) * (x - m);
		m = m + (x - m) / N;
	}

	public double mean() {
		return m;
	}

	public double var() {
		return s / (N - 1);
	}

	public double stddev() {
		return Math.sqrt(this.var());
	}

	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		
		Accumulator a = new Accumulator();
		for(int i = 0; i < n; i++) {
			double value = StdRandom.uniform(0.0, 1.0);
			StdDraw.setPenRadius(0.005);
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.point((double)i / n, value);
			a.addDataValue(value);			
			StdDraw.setPenRadius(0.006);
			StdDraw.setPenColor(StdDraw.BLUE);
			StdDraw.point((double)i / n, a.mean());

		}
	}
}
