class Rational {
	private final long numerator;
	private final long denominator;

	private long gcd(long p, long q) {
		if(q == 0)
			return p;
		long r = p % q;
		return gcd(q, r);
	}

	public Rational(long numerator, long denominator) {
		//
		assert numerator <= 1000 : "Numaratorul mai mare decat 1000";
		//
		assert denominator <= 1000 : "Numitorul mai mare decat 1000";

		long sign = (numerator * denominator >= 0? 1 : -1);
		numerator = Math.abs(numerator);
		denominator = Math.abs(denominator);
		long div = gcd(numerator, denominator);
		this.numerator = sign * numerator / div;
		this.denominator = denominator / div;
	}

	public Rational plus(Rational b) {
		long div = gcd(denominator, b.denominator);
		return new Rational(numerator * b.denominator / div + 
			b.numerator * denominator / div, denominator * b.denominator / div); 
	}

	public Rational minus(Rational b) { 
		long div = gcd(denominator, b.denominator);
		return new Rational(numerator * b.denominator / div - 
			b.numerator * denominator / div, denominator * b.denominator / div); 
	}

	public Rational times(Rational b) {
		return new Rational(numerator * b.numerator, denominator * b.denominator);
	}

	public Rational divides(Rational b) {
		return times(new Rational(b.denominator, b.numerator));
	}

	public boolean equals(Object o) {
		if(!(o instanceof Rational))
			return false;
		Rational that = (Rational)o;
		return numerator == that.numerator && denominator == that.denominator;
	}

	public String toString() {
		if(numerator == 0)
			return "0";
		if(denominator == 1)
			return  "" +numerator;
		return  numerator + "/" + denominator;
	}
}

public class e17 {
	public static void main(String[] args) {
		long a = Integer.parseInt(args[0]);
		long b = Integer.parseInt(args[1]);
		long c = Integer.parseInt(args[2]);
		long d = Integer.parseInt(args[3]);

		Rational r1 = new Rational(a,b);
		Rational r2 = new Rational(c,d);
		
		StdOut.println("r1: " + r1);
		StdOut.println("r2: " + r2);

		StdOut.println("r1 + r2 = " + r1.plus(r2));
		StdOut.println("r1 - r2 = " + r1.minus(r2));		

		StdOut.println("r2 + r1 = " + r2.plus(r1));
		StdOut.println("r2 - r1 = " + r2.minus(r1));

		StdOut.println("r1 * r2 : " + r1.times(r2));
		StdOut.println("r1 / r2 : " + r1.divides(r2));

		StdOut.println("r2 * r1 : " + r2.times(r1));
		StdOut.println("r2 / r1 : " + r2.divides(r1));

		StdOut.println("r1 + r2 == r2 + r1 : " + r1.plus(r2).equals(r2.plus(r1)));
		StdOut.println("r1 - r2 == r2 - r1 : " + r1.minus(r2).equals(r2.minus(r1)));
	}
}
