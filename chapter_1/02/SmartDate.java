class IllegalDateException extends Exception { }

public class SmartDate {
	private final int day;
	private final int month;
	private final int year;

	public SmartDate(int day, int month, int year) throws IllegalDateException {
		if(month < 1 || month > 12  || day < 1 || day > 31 || year < 0)
			throw new IllegalDateException();
		if(month == 2) {
			if(year % 4 == 0) {
				if(day > 29)
					throw new IllegalDateException();
			} else if(day > 28)
					throw new IllegalDateException();
		} else if(month == 4 || month == 6 || month == 9 || month == 11) {
			if(day == 31 ) 
				throw new IllegalDateException();
		}

		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int day() {
		return day;
	}

	public int month() {
		return month;
	}

	public int year() {
		return year;
	}

	public String toString() {
		return day()+"." + month() +"." + year();
	}

	public String dayOfTheWeek() {
		int a = (14 - month) / 12;
		int y = year - a;
		int m = month + 12 * a - 2;
		int d = (day + y + y / 4 - y / 100 + y / 400 + (31 * m /12)) % 7;
		switch(d) {
			case 0: return "Duminica";
			case 1: return "Luni";
			case 2: return "Marti";
			case 3: return "Miercuri";
			case 4: return "Joi";
			case 6: return "Vineri";
			case 7: return "Sambata";
			default: return "";
		}
	}

	public static void main(String[] args) {
		SmartDate sd = null;
		int error_count = 0;
		do {
			try {
				sd = new SmartDate(StdRandom.uniform(-5, 35), StdRandom.uniform(-5, 15),
					StdRandom.uniform(1900, 2100));
			} catch(IllegalDateException e) {
				error_count++;
			}
		} while(sd == null);
		StdOut.println(sd.dayOfTheWeek() + ", " + sd + " after " + error_count + " errors");
	}
}
