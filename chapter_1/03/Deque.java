/* Exercise 33 */
import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {
	int N = 0;
	Node first;
	Node last;

	private class Node {
		Item item;
		Node prev;
		Node next;
	}

	public boolean isEmpty() { return N == 0; }
	public int size() { return N; }

	void pushLeft(Item item) {
		Node oldfirst = first;
		first = new Node();
		first.item = item;

		if(isEmpty())
			last = first;
		else {
			first.next = oldfirst;
			oldfirst.prev = first;
		}

		N++;
	}

	void pushRight(Item item) {
		Node oldlast = last;
		last = new Node();
		last.item = item;
		
		if(isEmpty())
			first = last;
		else {
			last.prev = oldlast;
			oldlast.next = last;
		}

		N++;
	}

	Item popLeft() {
		Item item = first.item;
		first = first.next;
		N--;
		if(isEmpty())
			last = first;
		else
			first.prev = null;
		return item;
	}

	Item popRight() {
		Item item = last.item;
		last = last.prev;
		N--;
		if(isEmpty())
			first = last;
		else
			last.next = null;
		return item;
	}

	public Iterator<Item> iterator() {
		return new Iterator<Item>() {
			Node current = first;

			public boolean hasNext() {
				return current != null;
			}

			public Item next() {
				Item item = current.item;
				current = current.next;
				return item;
			}

			public void remove() { }
		};
	}

	public static void main(String[] args) {
		Deque<String> d = new Deque<String>();
		int k = 0;
		while(!StdIn.isEmpty()) {
			if(k++ % 2 == 0 )
				d.pushLeft(StdIn.readString());
			else
				d.pushRight(StdIn.readString());
		}
		while(d.size() > 0) {
			StdOut.print( d.popLeft() + " ");
		}
		StdOut.println();
	}
}
