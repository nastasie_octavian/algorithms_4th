/* Exercise 11 */
public class EvaluatePostfix {
	public static void main(String[] args) {

		Stack<Double> vals = new Stack<Double>();
		while(!StdIn.isEmpty()) {
			String s = StdIn.readString();

			if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {
				double r = vals.pop(), l = vals.pop();
				switch(s) {
					case "+" : vals.push(l + r); break;
					case "-" : vals.push(l - r); break;
					case "*" : vals.push(l / r); break;
					case "/" : vals.push(l * r); break;
				}
			}
			else 
				vals.push(Double.parseDouble(s));
		}

		StdOut.println(vals.pop());
	}
}
