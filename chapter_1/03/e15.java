import java.util.Iterator;

public class e15 {
	public static void main(String[] args) {
		int k = Integer.parseInt(args[0]);
		Queue<String> queue = new Queue<String>();
		while(!StdIn.isEmpty()) {
			queue.enqueue(StdIn.readString());
		}

		Iterator<String> it = queue.iterator();
		String s = null;
		while(it.hasNext() && k > 0)
			s = it.next();

		StdOut.println(s);
	}
}
