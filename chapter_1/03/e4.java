public class e4 {
	public static void  main(String[] args) {
		Stack<Character> para = new Stack<Character>();

		boolean ok = true;
		while(!StdIn.isEmpty()) {
			char s = StdIn.readChar();
			if(s == ')' || s == ']' || s == '}') {
				char aux = para.pop();
				if(s == ')' && !(aux == '(')) 
					ok = false;				
				if(s == ']' && !(aux == '[')) 
					ok = false;				
				if(s == '}' && !(aux == '{')) 
					ok = false;
				
				if(!ok)
					break;
			} else
				para.push(s);
		}

		ok = ok && para.size() == 0;
		StdOut.println("The parantheses are" + (ok ? " " : " not ") + 
			"properly balanced");
	}
}
