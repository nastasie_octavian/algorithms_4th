/* Exercise 40 */
public class MoveToFront<Item> {
	Node first;
	
	private class Node {
		Item item;
		Node next;
	}

	public void add(Item item) {
		if(first != null) {
			Node current = first;
			while(current.next != null) {
				if(current.next.item.equals(item))
					current.next = current.next.next;
				current = current.next;
			}
		}
		if(first == null || !first.item.equals(item)) {
				Node oldfirst = first;
				first = new Node();
				first.item = item;
				first.next = oldfirst;
		}
	}

	public void display() {
		Node current = first;
		while(current != null) {
			StdOut.print(current.item + " ");
			current = current.next;
		}
		StdOut.println();
	}

	public static void main(String[] args) {
		MoveToFront<Character> mtf = new MoveToFront<Character>();
		while(!StdIn.isEmpty()) {
			mtf.add(StdIn.readChar());
		}
		mtf.display();
	}
}
