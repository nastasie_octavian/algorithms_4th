import java.util.Iterator;

public class ex12 {
	
	public static <T> Stack<T> copy(Stack<T> stack)  {
		Stack<T> copy = new Stack<T>();
		Stack<T> aux = new Stack<T>();

		Iterator<T> it = stack.iterator();
		while(it.hasNext()) {
			aux.push(it.next());
		}

		while(aux.size() > 0)
			copy.push(aux.pop());

		return copy;
	}

	public static void main(String[] args) {
		Stack<String> stack = new Stack<String>();
		while(!StdIn.isEmpty())
			stack.push(StdIn.readString());

		Stack copy = copy(stack);
		while(copy.size() > 0)
			StdOut.println(copy.pop());
	}
}
