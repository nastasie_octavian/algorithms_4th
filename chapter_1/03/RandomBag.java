/* Exercise 34 */
import java.util.Iterator;

public class RandomBag<Item> implements Iterable<Item> {
	private int count;

	private class Node {
		Item item;
		Node next;
	}
	private Node first;
	
	public boolean isEmpty() { return count == 0; }
	public int size() { return count; }

	void add(Item item) { 
		Node oldfirst = first;
		first = new Node();
		first.item = item;
		first.next = oldfirst;
		count++;
	}

	@SuppressWarnings("unchecked")
	public Iterator<Item> iterator() {
		return new Iterator<Item>() {
			Item[] items = (Item[]) new Object[count];
			int i;
			
			{
				int k = 0;
				Node current = first;
				while(current != null) {
					items[k++] = current.item;
					current = current.next;
				}
				for(k = count - 1; k > 0; k--) {
					int j = StdRandom.uniform(0, k+1);
					Item temp = items[j];
					items[j] = items[k];
					items[k] = temp;
				}
			}

			public boolean hasNext() {
				return i < count;
			}

			public Item next() {
				return items[i++];
			}

			public void remove() { }
		};
	}

	public static void main(String[] args) {
		RandomBag<String> rb = new RandomBag<String>();
		while(!StdIn.isEmpty()) {
			rb.add(StdIn.readString());
		}

		Iterator<String> it = rb.iterator();
		while(it.hasNext()) {
			StdOut.print(it.next() + " ");
		}
		StdOut.println();

		it = rb.iterator();		
		while(it.hasNext()) {
			StdOut.print(it.next() + " ");
		}
		StdOut.println();
	}
}
