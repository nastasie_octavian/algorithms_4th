/* exercise 32 */

import java.util.Iterator;

public class Steque<Item> implements Iterable<Item> {
	private Node first;
	private Node last;
	private int N;

	private class Node {
		Item item;
		Node next;
	}

	public boolean isEmpty() { return first == null; }
	public int size()        { return N; }

	public void enqueue(Item item) {
		Node oldlast = last;
		last = new Node();
		last.item = item;

		if(isEmpty()) 
			first = last;
		else          
			oldlast.next = last;
		N++;
	}

	public Item pop() {
		Item item = first.item;
		first = first.next;
		if(isEmpty()) last = null;
		N--;
		return item;
	}

	public void push(Item item) {
		Node oldfirst = first;
		first = new Node();
		first.item = item;
		if(isEmpty())
			last = first;
		else
			first.next = oldfirst;
		N++;
	}

	public Iterator<Item> iterator() {
		return new ListIterator();
	}

	private class ListIterator implements Iterator<Item> {
		private Node current = first;

		public boolean hasNext() { return current != null; }
		public void remove() { }

		public Item next() {
			Item item = current.item;
			current = current.next;
			return item;
		}
	}

	public static void main(String[] args) { 
		Steque<String> q = new Steque<String>();
		int i = 0;
		while (!StdIn.isEmpty()) {
			String item = StdIn.readString();
			if (!item.equals("-")) {
				if(i++ % 2 == 0)
					q.enqueue(item);
				else 
					q.push(item);
			}
			else if (!q.isEmpty()) StdOut.print(q.pop() + " ");
		}
		StdOut.println("(" + q.size() + " left on queue)");
	}
}
