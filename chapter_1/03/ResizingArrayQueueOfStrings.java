/* Exercise 14 */
public class ResizingArrayQueueOfStrings {
	String[] items = new String[1];
	int capacity = 0;
	int first = 0, last = 0;

	public void enqueue(String item) {
		if(capacity == items.length) resize(2 * items.length);
		items[last] = item;
		last = ++last % items.length;
		capacity++;
	}

	public String dequeue() {
		String item = items[first];
		items[first] = null;
		first = ++first % items.length;
		capacity--;
		if(capacity > 0 && capacity == items.length / 4 ) resize(items.length / 2);
		return item;
	}

	private void resize(int cap) {
		String[] temp = new String[cap];
		int k = 0;
		for(int i = first; i < last; i++)
			temp[k++] = items[i];
		
		items = temp;
	}

	public int size() {
		return capacity;
	}

	public static void main(String[] args) {

		ResizingArrayQueueOfStrings queue = new ResizingArrayQueueOfStrings();
		
		while(!StdIn.isEmpty()) {
			queue.enqueue(StdIn.readString());
		}

		while(queue.size() > 0) 
			StdOut.println(queue.dequeue());
	}
}
