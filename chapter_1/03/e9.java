public class e9 {
	public static void main(String[] args) {
		Stack<String> rez = new Stack<String>();
		while(!StdIn.isEmpty()) {
			String s = StdIn.readString();
			if(s.equals(")")) {
				String aux = rez.pop();
				aux = rez.pop() + " " + aux;
				aux = rez.pop() + " " + aux;
				aux = "( " + aux + " )";
				rez.push(aux);
			} else
				rez.push(s);
		}

		while(rez.size() > 0)
			StdOut.println(rez.pop());
	}
}
