/* Exercise 10 */
public class InfixToPostfix {

	public static int precedence(String s) {
		if(s.equals("("))
			return 1;
		if(s.equals("+") || s.equals("-"))
			return 2;
		return 3;
	}

	public static void main(String[] args) {
		Queue<String> postfix = new Queue<String>();
		Stack<String> ops = new Stack<String>();

		while(!StdIn.isEmpty()) {
			String s = StdIn.readString();
			if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {
				while(ops.size() > 0  && precedence(s) <= precedence(ops.peek())) 
					postfix.enqueue(ops.pop());
					
				ops.push(s);
			} 
			else if(s.equals("(")) { 
				ops.push(s);
			}
			else if(s.equals(")")) {
				while(!ops.peek().equals("(") ) {
					postfix.enqueue(ops.pop());
				}
				ops.pop();
			} else
				postfix.enqueue(s);
		}
		while(ops.size() > 0)
			postfix.enqueue(ops.pop());
		while(postfix.size() > 0) {
			StdOut.print(postfix.dequeue() + " ");
		}
		StdOut.println();
	}
}
