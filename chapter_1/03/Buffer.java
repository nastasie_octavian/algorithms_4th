/* Exercise 44 */
public class Buffer {
	private Stack<Character> left = new Stack<Character>();
	private Stack<Character> right = new Stack<Character>();
	private int count = 0;
	
	public void insert(char c) {
		left.push(c);
		count++;
	}

	public char delete() {
		count--;
		if(left.size() > 0)
			return left.pop();
		if(right.size() > 0)
			return right.pop();
		return '\0';
	}

	void left(int k) {
		while(k-- > 0)
			right.push(left.pop());
	}

	void right(int k) {
		while(k-- > 0)
			left.push(right.pop());
	}

	public int size() {
		return count;
	}

	public static void main(String[] args) {
		Buffer buff = new Buffer();
		while(!StdIn.isEmpty()) {
			buff.insert(StdIn.readChar());
		}
		int k = 0;
		while(buff.size() > 0) {
			if(k++ % 2 == 0)
				buff.left(buff.size()-1);
			else
				buff.right(buff.size()-1);
			StdOut.print(buff.delete());
		}
		StdOut.println();
	}
}
