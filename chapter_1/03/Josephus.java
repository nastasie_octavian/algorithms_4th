/* Exercise 37 */
public class Josephus {
	public static void main(String[] args) {
		Queue<Integer> q = new Queue<Integer>();
		int N = Integer.parseInt(args[0]);
		int M = Integer.parseInt(args[1]);
		for(int i = 0; i < N; i++)
			q.enqueue(i);

		int k = 1;
		while(q.size() > 0) {
			if(k++ % M == 0)
				StdOut.print(" " + q.dequeue());
			else
				q.enqueue(q.dequeue());
		}
		StdOut.println();
	}
}
