/* Exercise 39 */

public class RingBuffer<Item> {
	int head = 0, tail = 0, N, size = 0;
	Item[] data;

	@SuppressWarnings("unchecked")
	public RingBuffer(int n) {
		N = n;
		data = (Item[]) new Object[n];
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public boolean isFull() {
		return size == N;
	}

	public void write(Item item) {
		if(!isFull()) {
			data[head] = item;
			head = ++head % N;
			size++;
		}
	}

	public Item read() {
		if(!isEmpty()) {
			Item item = data[tail];
			tail = ++tail % N;
			size--;
			return item;
		}
		return null;
	}
	

	public static void main(String[] args) {
		RingBuffer<String> rb = new RingBuffer<String>(Integer.parseInt(args[0]));
		while(!StdIn.isEmpty()) {
			if(!rb.isFull())
				rb.write(StdIn.readString());
			else {
				while(!rb.isEmpty())
					StdOut.print(rb.read() + " ");
				StdOut.print("# ");
			}
		}
		while(!rb.isEmpty())
				StdOut.print(rb.read() + " ");
		StdOut.println();
	}
}
