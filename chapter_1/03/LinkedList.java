import java.util.Iterator;

public class LinkedList<Item> implements Iterable<Item> {
	private Node first;
	private Node last;
	private int N;

	private class Node {
		Item item;
		Node next;
	}

	public boolean isEmpty() { return first == null; }
	public int size()        { return N; }

	public void addLast(Item item) {
		Node oldlast = last;
		last = new Node();
		last.item = item;

		if(isEmpty()) 
			first = last;
		else          
			oldlast.next = last;
		N++;
	}

	public Item removeFirst() {
		Item item = first.item;
		first = first.next;
		if(isEmpty()) last = null;
		N--;
		return item;
	}

	public Iterator<Item> iterator() {
		return new ListIterator();
	}

	private class ListIterator implements Iterator<Item> {
		private Node current = first;

		public boolean hasNext() { return current != null; }
		public void remove() { }

		public Item next() {
			Item item = current.item;
			current = current.next;
			return item;
		}
	}

	public static LinkedList<String> readStringList() {
		LinkedList<String> list = new LinkedList<String>();
		while(!StdIn.isEmpty()) {
			list.addLast(StdIn.readString());
		}
		return list;
	}

	public static LinkedList<Integer> generateIntList(int N) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		for(int i = 0; i < N; i++)
			list.addLast(StdRandom.uniform(100));
		return list;
	}
	
	public static <T> LinkedList<T> copyList(LinkedList<T> list) {
		LinkedList<T> copyList = new LinkedList<T>();
		
		Iterator<T> it = list.iterator();
		while(it.hasNext())
			copyList.addLast(it.next());
		return copyList;
	}

	public static <T> void display(LinkedList<T> list) {		
	
		Iterator<T> it = list.iterator();
		while(it.hasNext())
			StdOut.print(it.next() + " ");
		StdOut.println();
	}

	public boolean delete(int k) {

		if(k == 0) { 
			removeFirst(); 
		} else {
			Node current = first;
			while(current.next != null && k > 1) {
				current = current.next;
				k--;
			}
			if(k > 1)
				return false;

			if(current.next != null)
				current.next = current.next.next;
		}
		return true;
	}

	public boolean find(Item key) {
		Node current = first;
		while(current != null) {
			if(current.item.equals(key))
				return true;
			current = current.next;
		}
		return false;
	}

	public void removeAfter(Node node) {
		if(node == null || node.next == null) return;
		node.next = node.next.next;
	}

	public void insertAfter(Node node1, Node node2) {
		if(node1 == null | node2 == null) return;

		node2.next = node1.next;
		node1.next = node2;
	}

	public static int max(LinkedList<Integer> list) {
		Iterator<Integer> it = list.iterator();
		if(!it.hasNext()) return 0;
		int max = it.next();
		while(it.hasNext()) {
			int aux = it.next();
			if(max < aux)
				max = aux;
		}
		return max;
	}

	public static int maxR(LinkedList<Integer> list) {
		if(list.size() == 1)
			return list.removeFirst();

		int max = list.removeFirst();
		int max2 = maxR(list);
		if(max > max2)
			return max;
		return max2;
	}

	public void remove(Item key) {
		while(first.equals(key)) removeFirst();
		if(first != null) {
			Node current = first;
			while(current.next != null) {
				if(current.next.item.equals(key))
					removeAfter(current);
				else
					current = current.next;
			}
		}
	}

	public static void main(String[] args) {
		LinkedList<String> listS = readStringList();
		display(listS);

		for(int i = 0; i < listS.size(); i++ ) {
			LinkedList<String> copy = copyList(listS);
			copy.delete(i);
			StdOut.print("Delete elem " + i + ", result: ");
			display(copy);
		}

		if(args.length > 0)	{
			StdOut.println("Key \"" + args[0] + "\" exists in the list: " + 
				listS.find(args[0]));
			LinkedList<String> copy = copyList(listS);
			copy.remove(args[0]);
			StdOut.print("List after removing the key \"" + args[0] +"\" : ");
			display(copy);
		}

		LinkedList<Integer> listI = generateIntList(10);
		display(listI);
		StdOut.println("Maximum value in list: " + max(listI));
		StdOut.println("Rec maximum value in list: " + max(copyList(listI)));
	}
}
