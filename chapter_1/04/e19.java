/*
	Am tratat matricea ca un vector, si am aplicat acelasi algoritm ca la ex precedent.
*/

import java.util.Arrays;

public class e19 {
	
	public static int[] localMin(int[][] a) {
		int mid, lo, hi, i, j;
		lo = 0; hi = a.length * a.length - 1;
		while(lo < hi) {
			mid = (lo + hi) / 2;
			i = (mid - 1) / a.length;
			j = mid  % a.length;
			int i1 = (j == 0 ? i - 1 : i);
			if(i1 < 0)
				return null;
			int i2 = (j == a.length - 1 ? i + 1 : i);
			int j1 = (j == 0 ? a.length -1 : j-1);
			int j2 = (j == a.length -1 ? 0 : j + 1);
			if(a[i1][j1] >= a[i][j] && a[i][j] <= a[i2][j2] && i1 == i2)
				return new int[] { i, j };
			else if( a[i1][j1] <= a[i2][j2])
				hi = mid -1;
			else
				lo = mid + 1;
		}

		return null;
	}

	public static void main(String[] args) {
		int N = StdIn.readInt();	
		int[][] a = new int[N][N];
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				a[i][j] = StdIn.readInt();

		StdOut.println(Arrays.deepToString(a));
		StdOut.println(Arrays.toString(localMin(a)));
	}
}
