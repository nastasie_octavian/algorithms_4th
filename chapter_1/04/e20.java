/*
	3 functii de cautare binara, una care gaseste punctul de maxim,
	una pt portiunea ascendenta si una pt cea descendenta.
*/

public class e20 {

	public static int rank(int key, int[] a) {
		int rez, top = rankTop(a);
		rez = rankUp(key, a, 0, top);
		if(rez < 0)
			rez = rankDown(key, a, top, a.length - 1);

		return rez;
	}


	public static int rankUp(int key, int[] a, int lo, int hi) {
		if(lo > hi)
			return -1;

		int mid = (lo + hi) / 2;
		if(key < a[mid]) return rankUp(key, a, lo, mid - 1);
		else if(key > a[mid]) return rankUp(key, a, mid + 1, hi);
		else return mid;
	}
	
	public static int rankDown(int key, int[] a, int lo, int hi) {
		if(lo > hi)
			return -1;

		int mid = (lo + hi) / 2;
		if(key > a[mid]) return rankDown(key, a, lo, mid - 1);
		else if(key < a[mid]) return rankDown(key, a, mid + 1, hi);
		else return mid;
	}

	private static int rankTop(int[] a) {
		int lo = 0, hi = a.length - 1, mid;
		while(lo < hi) {
			mid = (lo + hi) /2;
			if( a[mid] > a[mid-1] && a[mid] > a[mid+1])
				return mid;
			if(a[mid] > a[mid-1])
				lo = mid + 1;
			else
				hi = mid + 1;
				
		}
		return lo;
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		StdOut.println(rankTop(a));
		while(!StdIn.isEmpty())
			StdOut.println("rank: " + rank(StdIn.readInt(), a));
	}
}
