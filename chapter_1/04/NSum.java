/*
	Program pt n-sum, java NSum nr_nr_pereche fisier nr, ordin de crestere ~N^(n-1) (cred)
*/

import java.util.Arrays;

class IntWrap {
	public int val;
}

public class NSum {

	public static int count(int[] a, int n) {
		IntWrap cnt = new IntWrap();
		cnt.val = 0;
		IntWrap N = new IntWrap();
		N.val = a.length;
		int[] ind = new int[n-1];
		for(ind[0] = 0; ind[0] < N.val; ind[0]++)
			count(a, ind, 1, cnt, N);
		return cnt.val;
	}

	private static void count(int[] a, int[] ind, int i, IntWrap cnt, IntWrap N) {
		if(i < ind.length) {
			for(ind[i] = ind[i-1] + 1; ind[i] < N.val; ind[i]++)
				count(a, ind, i+1, cnt, N);
		} else {
			int sum = 0;
			for(int j = 0; j < ind.length; j++)
				sum += a[ind[j]];
			int aux = BinarySearch.rank(-sum, a);
			
			if(aux > ind[i-1]) {
				cnt.val++;
				N.val = aux;
			}
		}
	}

	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int[] a = In.readInts(args[1]);
		Arrays.sort(a);
		StdOut.println(count(a, n));
	}
}
