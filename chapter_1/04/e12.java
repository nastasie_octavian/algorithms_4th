public class e12 {
	
	public static void main(String[] args) {
		int a[] = In.readInts(args[0]);
		int b[] = In.readInts(args[1]);
		for(int i = 0; i < a.length; i++) {
			if(BinarySearch.rank(a[i], b) != -1)
				StdOut.println(a[i] + " ");
			while(i < a.length - 1 && a[i] == a[i+1])
				i++;
		}
	}
}
