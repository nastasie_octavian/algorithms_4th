/*
	Solutia e ~Nlog(N), nu cred ca exista una mai buna
*/

import java.util.Arrays;

public class e16 {
	public static int[] pair(int[] a) {
		double min = Math.abs(a[1] - a[0]);
		int i = 0, j = 1;
		for(int k = 1; k < a.length -1; k++)
			if( min > Math.abs(a[k+1] - a[k])) {
				min = Math.abs(a[k+1] - a[k]);
				i = k;
				j = k+ 1;
			}
		return new int[] { i, j};
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		Arrays.sort(a);
		int[] aux = pair(a);
		StdOut.println( "[" + a[aux[0]]+ ", " + a[aux[1]] + "]");
	}
}
