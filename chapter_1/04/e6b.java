public class e6b {
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int sum = 0;
		for(int i = 1; i < N; i *= 2)
			for(int j = 0; j < i; j++)
				sum++;
		StdOut.println("Sum: " + sum);
	}
}
