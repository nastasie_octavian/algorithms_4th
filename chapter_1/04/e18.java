public class e18 {

	public static int localMinBook(int[] a) {
		int lo, hi, mid;
		lo = 0; hi = a.length-1;
		while(lo <= hi) {
			mid =  (hi + lo) / 2;
			if(a[mid] >= a[mid-1] && a[mid] <= a[mid+1])
				return mid;
			else if(a[mid-1] < a[mid+1])
				hi = mid - 1;
			else
				lo = mid + 1;
		}
		return -1;
	}

	public static int localMinCount(int[] a) {
		int cnt = 0;
		for(int i =1; i < a.length -1; i++)
			if(a[i-1] >= a[i] && a[i] <= a[i+1])
				cnt++;
		return cnt;
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		int rez = localMinBook(a);
		StdOut.println("Count: " + localMinCount(a));
		if(rez != -1)
			StdOut.println(a[rez]);
	}
}
