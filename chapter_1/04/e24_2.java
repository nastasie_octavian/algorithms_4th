public class e24_2 {
	public static int find(int target, int N) {
		int fk = 1, fk1 = 0;

		while(fk < target)  {
			int aux = fk + fk1;
			fk1 = fk;
			fk = aux;
		}

		int lo = fk1;
		int hi = fk;
		while(lo <= hi) {
			int fk2 = fk - fk1;
			if(target == lo +fk2)
				return lo + fk2;
			if(target > lo + fk2) {
				lo += fk2;
			}
			else 
				hi = lo + fk2;				
			fk= fk1;
			fk1 = fk2;
		}

		return -1;
	}

	public static void main(String[] args) {
		Stopwatch s = new Stopwatch();
		StdOut.println(find(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
		StdOut.println("Time: " + s.elapsedTime());
	}
}
