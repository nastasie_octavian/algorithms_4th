/*
	Nu stiu daca asta e solutia, am incercat ceva care ar fi 2lg(F), dar nu stiu daca e bine
*/

public class e24 {
	public static int find(int target, int N) {
		int hi = 1;

		while(hi  < target) 
			hi *= 2;
		int lo = hi / 2;
		while(lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			if(mid == target)
				return mid;
			if(mid < target) lo = mid + 1;
			else hi = mid - 1;
		}
		return -1;
	}

	public static void main(String[] args) {
		Stopwatch s = new Stopwatch();
		StdOut.println(find(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
		StdOut.println("Time: " + s.elapsedTime());
	}
}
