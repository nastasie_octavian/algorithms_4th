/* 
	M-am jucat si eu un pic cu Strategy Pattern, e tot algoritmul de la 20, numai
	ca exista o singura functie de cautare pe intervalul credcator, resp descrescator
	care isi modifica comportamentul in functie de obiectul strategie (monotonie interval).
*/

interface Cmp {
	boolean smaller(int key, int val);
	boolean bigger(int key, int val);
}

class Up implements Cmp {	
	public boolean smaller(int key, int val) {
		return key < val;
	}

	public boolean bigger(int key, int val) {
		return key > val;
	}
}

class Down implements Cmp {	
	public boolean smaller(int key, int val) {
		return key > val;
	}

	public boolean bigger(int key, int val) {
		return key < val;
	}
}

public class e20_2 {

	public static int rank(int key, int[] a) {
		int rez, top = rankTop(a);
		rez = rank(key, a, 0, top, new Up());
		if(rez < 0)
			rez = rank(key, a, top, a.length - 1, new Down());

		return rez;
	}


	public static int rank(int key, int[] a, int lo, int hi, Cmp cmp) {
		if(lo > hi)
			return -1;

		int mid = (lo + hi) / 2;
		if(cmp.smaller(key, a[mid])) return rank(key, a, lo, mid - 1, cmp);
		else if(cmp.bigger(key, a[mid])) return rank(key, a, mid + 1, hi, cmp);
		else return mid;
	}

	private static int rankTop(int[] a) {
		int lo = 0, hi = a.length - 1, mid;
		while(lo < hi) {
			mid = (lo + hi) / 2;
			if( a[mid] > a[mid-1] && a[mid] > a[mid+1])
				return mid;
			if(a[mid] > a[mid-1])
				lo = mid + 1;
			else
				hi = mid + 1;
				
		}
		return lo;
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		StdOut.println(rankTop(a));
		while(!StdIn.isEmpty())
			StdOut.println("rank: " + rank(StdIn.readInt(), a));
	}
}
