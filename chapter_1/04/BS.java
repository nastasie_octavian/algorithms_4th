/* 
	Teoretic asta ar fi exercitiul 22, practic, nu functioneaza.
*/

import java.util.Arrays;

public class BS {

	public static int rank(int key, int[] a) { 
		return rank(key, a, 0, a.length - 1);
	}

	public static int rank(int key, int[] a, int lo, int hi) { 
		if (lo > hi) return -1;
		int mid = lo + (hi - lo) / 2;
		if(key < a[mid]) return rank(key, a, lo, mid - 1);
		else if (key > a[mid]) return rank(key, a, mid + 1, hi);
		else return mid;
	}

	public static int rankMP(int key, int[] a) {
		int fk1 = 1, fk = 1, fk2 = 0;
		while(fk < a.length) {
			int aux = fk;
			fk = fk + fk1;
			fk1 = aux;
		}
		int aux = fk - fk1;
		fk = fk1;
		fk1 = aux;

		int lo = 0;
		int hi = lo + fk;
		
		while(lo <= hi && fk1 > 0) {
			fk2 = fk - fk1;
			if( key < a[lo + fk2]) {
				hi = lo + fk2;
				fk1 = fk1 - fk2;
				fk = fk2;
			} else if(key > a[lo + fk2]) {
				hi = lo + fk2 + fk1;
				lo = lo + fk2;
				fk = fk1;
				fk1 = fk2;
			} else return lo + fk2;
		}
		return -1;
	}
		
    public static void main(String[] args) {
		StdOut.println("NOT Functional!");
		System.exit(0);
		int N = Integer.parseInt(args[0]);
		int[] a = new int[N];

		for(int i = 0; i < N; i++)
			a[i] = StdRandom.uniform(-10000, 10000);
        Arrays.sort(a);

		int[] searches = new int[N/10];
		for(int i = 0; i < N/10; i++)
			searches[i] = StdRandom.uniform(-10000, 10000);

		int count = 0;
		Stopwatch s = new Stopwatch();
		for(int i = 0; i < searches.length; i++)
			if(rank(searches[i], a) != -1)
				count++;
		double time = s.elapsedTime();
		StdOut.printf("%7d %7.3f\n", count, time);

		count = 0;
		s = new Stopwatch();
		for(int i = 0; i < searches.length; i++)
			if(rankMP(searches[i], a) != -1)
				count++;
		time = s.elapsedTime();
		StdOut.printf("%7d %7.3f\n", count, time);
    }
}
