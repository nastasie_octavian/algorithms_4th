import java.util.ArrayList;
import java.util.Iterator;

/*
	Experiment, sa vad care e faza cu ordinele de crestere pt 1.4.6
	a. si b. ar trebui sa fi echivalente, T(N) = ~2N, numai ca, in cazul b.
	daca N nu este 2^k + 1, atunci nu se mai aduna N la sfarsit, deci creste doar cu ~N, 
	in timp ce in cazul a. intodeuna se aduna N la inceput, deci creste intodeauna cu 
	~2N.
*/
public class e6e {
	public static int a(int N) {
		int sum = 0;
		for(int n = N; n > 0; n /= 2)
			for(int i = 0; i < n; i++)
				sum++;
		return sum;
	}

	public static int b(int N) {	
		int sum = 0;
		for(int i = 1; i < N; i *= 2)
			for(int j = 0; j < i; j++)
				sum++;
		return sum;
	}
	
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		ArrayList<Integer> sums = new ArrayList<Integer>();
		StdDraw.setXscale(0, N);
		int max = 0;
		StdDraw.setPenRadius(0.005);
		for(int n = 0; n <= N; n++) {
			sums.add(a(n));
			sums.add(b(n));
			StdDraw.clear();
			int count = 1;
			Iterator<Integer> it = sums.iterator();
			while(it.hasNext()) {
				int sum = it.next();
				if(sum > max) {
					StdDraw.setYscale(0, sum + 1); 
					max = sum;
				}
				StdDraw.setPenColor(StdDraw.BLUE);
				StdDraw.point(count, sum);

				sum = it.next();
				if(sum > max) {
					StdDraw.setYscale(0, sum + 1);
					max = sum;
				}
				StdDraw.setPenColor(StdDraw.RED);
				StdDraw.point(count, sum);
				count++;
			}
		}
	}
}
