public class e19Test {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int N = Integer.parseInt(args[1]);
		int[][] a = new int[N][N];
		int local = 0;
		for(int k = 0; k < n; k++) {
			for(int i = 0; i < N; i++)
				for(int j = 0; j < N; j++)
					a[i][j] = StdRandom.uniform(-10000, 10000);
			while(a[0][0] < a[0][1])
				a[0][0] = StdRandom.uniform(-10000, 10000);			
			while(a[N-1][N-1] < a[N-1][N-2])
				a[N-1][N-1] = StdRandom.uniform(-10000, 10000);

			if(e19.localMin(a) != null)
				local++;
		}
		StdOut.println("Rez: " + local);
	}
}
