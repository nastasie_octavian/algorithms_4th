/*
	Exercise 11
*/

import java.util.Arrays;

public class StaticSETOfInts {
	private int[] a;

	public StaticSETOfInts(int[] keys) {
		a = new int[keys.length];
		for (int i = 0; i < keys.length; i++)
			a[i] = keys[i]; // defensive copy
		Arrays.sort(a);
	}

	public boolean contains(int key) { return rank(key) != -1; }

	private int rank(int key) { // Binary search.
		int lo = 0;
		int hi = a.length - 1;
		while (lo <= hi) { // Key is in a[lo..hi] or not present.
			int mid = lo + (hi - lo) / 2;
			if(key < a[mid]) hi = mid - 1;
			else if (key > a[mid]) lo = mid + 1;
			else return mid;
		}
		return -1;
	}

	private int rankLo(int key) {
		int lo = 0;
        int hi = a.length - 1;
		int pos = -1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
			if(a[mid] == key) {
				pos = mid;
			}
            if(key <= a[mid]) hi = mid - 1;
            else if(key > a[mid]) lo = mid + 1;
        }
        return pos;
	}
	
	private int rankHi(int key) {
		int lo = 0;
        int hi = a.length - 1;
		int pos = -1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
			if(a[mid] == key) {
				pos = mid;
			}
            if(key < a[mid]) hi = mid - 1;
            else if(key >= a[mid]) lo = mid + 1;
        }
        return pos;
	}

	public int howMany(int key) {
		int posHi = rankHi(key);
		int posLo = rankLo(key);
		if(posLo == -1)
			return 0;
		return posHi - posLo + 1;
	}

	public static void main(String[] args) {
		 int[] ints = In.readInts(args[0]);
		
		StaticSETOfInts set = new StaticSETOfInts(ints);
		while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            StdOut.println(set.howMany(key));
        }
	}
}
