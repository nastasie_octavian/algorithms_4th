/*
	~2lgN, nu gasesc unul care sa fie ~lgN, desi banuiesc ca nu are sens, ca, functioneaza
	perfect algoritmul de la eggs daca e nevoie.
*/
public class e34 {
	
	public static int guess(int target, int N) {
		int oclose = Integer.MAX_VALUE, close = 0;
		int olo = 0, lo = 0, hi = N, omid = 0, mid;

		while(lo <= hi) {
			mid = lo + (hi - lo) / 2;
			close = Math.abs(target - mid);
			if(close == 0)
				return mid;
			if(close <= oclose) {
				olo = lo;
				lo = mid;
			} else {
				hi = lo;
				lo = olo;
			}
			omid = mid;
			oclose = close;
		}

		return -1;
	}	

	public static void main(String[] args) {
		Stopwatch s = new Stopwatch();
		int rez  = guess(Integer.parseInt(args[0]), Integer.parseInt(args[0]));
		double time = s.elapsedTime();
		StdOut.println("Rez: " + rez + " Time: " + time);
	}
}
