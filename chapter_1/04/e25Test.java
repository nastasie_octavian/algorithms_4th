public class e25Test {

	public static int find(int target, int N) {
		int lo = 0, hi = N;
		while(lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			if(mid == target)
				return mid;
			else if(mid < target) lo = mid + 1;
			else hi = mid - 1;
		}
		return -1;
	}

	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int lim = 1;
		if(args.length > 1)
			lim = Integer.parseInt(args[1]);
		double mytime = 0, bstime = 0;

		for(int i = 0; i < n; i++) {
			int N = StdRandom.uniform(lim, 1000000000);
			int floor = StdRandom.uniform(0, N / lim);
			Stopwatch s = new Stopwatch();
			int rez1 = find(floor, N);
			bstime += s.elapsedTime();
			s = new Stopwatch();
			int rez2 = e25.find(floor, N);
			mytime += s.elapsedTime();	
			if(rez1 != rez2) {
				StdOut.println("Rezultate diferite: " + rez1 + " " + rez2 + 
					" " + N + " " + floor);
				System.exit(0);
			}
		}

		StdOut.printf("BS time: %7.4f\nMy time: %7.4f\nRatio: %7.5f\n", 
			bstime, mytime, mytime/bstime);
	}
}
