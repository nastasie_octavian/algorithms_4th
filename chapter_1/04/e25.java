public class e25 {

	public static int find(int target, int N) {
		int range = (int)Math.sqrt(N), i;
		for(i = 0; i < N; i+=range)
			if(i > target)
				break;
		for(int j = i - range; j < i; j++)
			if(j == target)
				return j;
		return -1;
	}

	public static void main(String[] args) {
		Stopwatch s = new Stopwatch();
		StdOut.println(find(Integer.parseInt(args[0]),Integer.parseInt(args[1])));
		StdOut.println("Time: " + s.elapsedTime());
	}
}
