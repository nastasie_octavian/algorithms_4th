public class ThreeSumDoubling  {
	public static int count(int[] a) {
		int N = a.length;
		int cnt = 0;

		for(int i = 0; i < N; i++)
			for(int j = i +1; j < N; j++)
				for(int k = j + 1; k < N; k++)
					if(a[i] + a[j] + a[k] == 0)
						cnt++;
		return cnt;
	}
	
	public static int countNaive(int[] a) {
		int N = a.length;
		int cnt = 0;
		
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				for (int k = 0; k < N; k++)
					if (i < j && j < k)
						if (a[i] + a[j] + a[k] == 0)
							cnt++;
		
		return cnt;
	}

	public static void main(String[] args) {
		double timeP=1, timeNP=1;
		StdOut.printf("%7s %7s %7s %7s %7s %7s %7s\n", "N", "Count", "Time", "Ratio", 
						"Count", "Time", "Ratio");
		for(int n = 250; true; n += n) {
			int[] a = new int[n];
			for(int i = 0; i < n; i++)
				a[i] = StdRandom.uniform(-1000000000, 1000000000);

			Stopwatch s = new Stopwatch();
			int rez = count(a);
			double time = s.elapsedTime();

			StdOut.printf("%7d %7d %7.4f %7.4f ", n, rez, time, time / timeP);
			timeP = time;

			s = new Stopwatch();
			rez = countNaive(a);
			double timeN = s.elapsedTime();

			StdOut.printf("%7d %7.4f %7.4f\n", rez, timeN, timeN / timeNP);
			timeNP = timeN;
		}
	}
}
