/*
	Chiar nu stiu daca aici am facut ce trebuie, enuntul e destul de ciudat, 
	poate se refera la cea mia apropiata fractia p/q < x, nu la una oarecare?
*/

import java.util.Arrays;

public class e23 {
	public static int[] find(double x, int N) {
		double key = x * (N-1);
		return new int[]  { rank(key, N-2), N -1};
	}

 	public static int rank(double key, int hi) {
		int lo = 0;
		while(lo <= hi) {
			int mid = (lo + hi) / 2;
			if(key > mid) return mid;
			else hi = mid - 1;
		}

		return -1;
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		StdOut.println(Arrays.toString(find(x, N)));
	}
}
