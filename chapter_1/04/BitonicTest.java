import java.util.*;


public class BitonicTest {
	public static boolean noDuplicate(int i, int[] a) {
		for(int j = 0; j < i; j++)
			if(a[j] == a[i])
				return false;
		return true;
	}

	public static void main(String[] args) {
		for(int n = 250; true; n*=2) {
			double time1 = 0, time2 = 0;
			for(int t = 0; t < 100; t++) {
				int[] a = new int[n];
				int top = StdRandom.uniform(1, n);
				
				Set<Integer> randoms = new LinkedHashSet<Integer>();
				while(randoms.size() < n) {
					randoms.add(StdRandom.uniform(-1000000000, 1000000000));
				}
	
				Iterator<Integer> it = randoms.iterator();
				int[] up = new int[top];
				for(int i = 0; i < top; i++)
					up[i] = it.next();
				Arrays.sort(up)	;

				int[] down = new int[n - top];
				for(int i = 0; i < n - top; i++)
					down[i] = it.next();
				Arrays.sort(down);

				int j = 0;
				for(int i = 0; i < top; i++)
					a[j++] = up[i];

				for(int i = down.length-1; i >= 0; i--)
					a[j++] = down[i];
	
				for(int i = 0; i < Math.sqrt(n); i++) {
					int pos = StdRandom.uniform(n);
					int search = a[pos];
					int rez1, rez2;
	
					Stopwatch s = new Stopwatch();
					rez1 = Bitonic.rank3lg(search, a);
					time1 += s.elapsedTime();				
	
					s = new Stopwatch();
					rez2 = Bitonic.rank2lg(search, a);
					time2 += s.elapsedTime();
	
					if(rez1 != pos || rez2 != pos) {
						StdOut.println("Eroare, rezultate gresite:\nlg3: "
								+rez1+"\nlg2: " + rez2 + "\n" + pos + " " + top + " " + n + "\n" 
								+ a[pos-1] + " " + a[pos] + " " + a[pos+1]);
						System.exit(0);
					}
				}
			}
			StdOut.printf("%8d %7f %7f\n", n, time1, time2);
		}
	}
}
