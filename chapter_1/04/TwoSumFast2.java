/*
	Avand in vedere ca se sorteaza vectorul, in momentul in care gasesc o pereche
	a carei suma este 0, stiu ca nu mia are sens sa caut si dupa aux, deoarece nu 
	o sa gasesc niciodata valori care sa indeplineasca conditia.
*/

import java.util.Arrays;

public class TwoSumFast2 {
	public static int count(int[] a) {
		Arrays.sort(a);
		int N = a.length;
		int cnt = 0;
		for(int i = 0; i < N; i++) {
			int aux = BinarySearch.rank(-a[i], a);
			if(aux > i) {
				cnt++;
				N = aux;
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		StdOut.println(count(a));
	}
}
