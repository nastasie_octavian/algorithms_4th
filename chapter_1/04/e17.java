import java.util.Arrays;

public class e17 {
	public static int[] pair(int[] a) {
		int mini = 0, maxi = 0; 
		for(int k = 1; k < a.length; k++) {
			if(a[mini] > a[k])
				mini = k;
			else if(a[maxi] < a[k])
				maxi = k;
		}
		return new int[] { mini, maxi};
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		int[] aux = pair(a);
		StdOut.println( "[" + a[aux[0]]+ ", " + a[aux[1]] + "]");
	}
}
