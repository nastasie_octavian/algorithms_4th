/*
	Exercise 44
*/

import java.util.ArrayList;

public class Birthday {
	
	public static int repeted(int N) {
		ArrayList<Integer> ints = new ArrayList<Integer>();
		int cnt = 0;
		int rand = StdRandom.uniform(0, N);
		while(!ints.contains(rand)) {
			cnt++;
			ints.add(rand);
			rand = StdRandom.uniform(0, N);
		}
		return cnt;
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);
		int rez = 0;
		for(int i = 0; i < n; i++)
			rez += repeted(N);
		StdOut.printf("Result: %10.5f ~ %10.5f\n", 
			(double)rez /  n, Math.sqrt(Math.PI * (double)N / 2));
	}
}
