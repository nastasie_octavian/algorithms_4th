public class e34Test {

	public static int find(int target, int N) {
		int lo = 0, hi = N;
		while(lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			if(mid == target)
				return mid;
			else if(mid < target) lo = mid + 1;
			else hi = mid - 1;
		}
		return -1;
	}

	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int lim = 1;
		if(args.length > 1)
			lim = Integer.parseInt(args[1]);
		double mytime1 = 0, bstime = 0;

		for(int i = 0; i < n; i++) {
			int N = StdRandom.uniform(lim, 1000000000);
			int floor = StdRandom.uniform(0, N / lim);

			Stopwatch s = new Stopwatch();
			int rez1 = find(floor, N);
			bstime += s.elapsedTime();

			s = new Stopwatch();
			int rez2 = e34.guess(floor, N);
			mytime1 += s.elapsedTime();				
		}

		StdOut.printf("BinaryS time: %7.4f\nGuess 1 time: %7.4f\n", bstime, mytime1);
	}
}
