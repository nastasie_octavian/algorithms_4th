public class e1_1_32 {
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		double l = Double.parseDouble(args[1]);
		double r = Double.parseDouble(args[2]);
		
		int[] count = new int[N];
		double width = (r - l) / N;

		while(!StdIn.isEmpty()) {
			double value = StdIn.readDouble();
			value -= l;
			int pos =(int)(value / width);
			if(pos < N && pos >= 0)
				count[pos]++;
		}
		for(int i = 0; i < N; i++) {
			StdDraw.filledRectangle(1.0 * i / N, 0, 0.5 / N, count[i]/20.0);
		}
	}
}
