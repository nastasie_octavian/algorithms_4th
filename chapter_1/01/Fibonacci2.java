import java.util.*;

public class Fibonacci2 {
	public static HashMap<Integer, Long> fibo = new HashMap<Integer, Long>();
	static {
		fibo.put(0, 0l);
		fibo.put(1, 1l);
	}

	public static long F(int N) {
		Long calc = fibo.get(N);
		if(calc != null)
			return calc;
		calc = F(N-1) + F(N-2);
		fibo.put(N, calc);
		return calc;
	}

	public static void main(String[] args) {
		int N = 100;
		if(args.length == 1)
			N = Integer.parseInt(args[0]);

		for(int i = 0; i < N; i++)
			StdOut.println(i +" " + F(i));
	}
}
