public class e1_1_14 {
	public static int log(int N) {
		int aux = 1;
		int x = 0;
		while(aux < N) {
			x++;
			aux *= 2;
		}
		if(aux != N)
			x--;

		return x;
	}

	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("Please supply number");
			System.exit(0);
		}
		int N = Integer.parseInt(args[0]);
		System.out.format("log2(%d) = %d\n", N, log(N));
	}
}
