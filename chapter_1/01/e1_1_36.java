interface IShuffle {
	public void shuffle(double[] a);
}

class GoodShuffle implements IShuffle {	
	public void shuffle(double[] a) {
		int N = a.length;
		for(int i = 0; i < N; i++) {
			int r = i + StdRandom.uniform(N-i);
			double temp = a[i];
			a[i] = a[r];
			a[r] = temp;
		}
	}
}

class BadShuffle implements IShuffle {	
	public void shuffle(double[] a) {
		int N = a.length;
		for(int i = 0; i < N; i++) {
			int r = StdRandom.uniform(N);
			double temp = a[i];
			a[i] = a[r];
			a[r] = temp;
		}
	}
}

public class e1_1_36 {

	public static void test(int M, int N, IShuffle shuffle) {		
		
		double[] a = new double[M];
		int[][] times = new int[M][M];

		for(int j = 0; j < N; j++) {
			for(int i = 0; i < M; i++)
				a[i] = i;
			shuffle.shuffle(a);
			for(int i = 0; i < M; i++)
				times[(int)a[i]][i]++;
		}
		
		System.out.print("    |");
		for(int i = 0; i < M; i++)
			System.out.format("%4s ", i);
		System.out.println("");
		for(int i = 0; i <= M; i++)
			System.out.print("-----");
		System.out.println("");

		for(int i = 0; i < M; i++) {
			System.out.format("%3d |", i);
			for(int j = 0; j < M; j++)
				System.out.format("%4d ", times[i][j]);
			System.out.println();
		}
	}

	public static void main(String[] args) {
		int M = Integer.parseInt(args[0]);
		int N = Integer.parseInt(args[1]);
	
		System.out.println("Good");
		test(M, N, new GoodShuffle());
		System.out.println("Bad");
		test(M, N, new BadShuffle());
	}
}
