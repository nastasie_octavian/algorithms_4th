import java.util.*;

public class e1_1_15 {
	public static int[] histogram(int[] a, int M) {
		int[] rez = new int[M];
		for(int i = 0; i < a.length; i++)
			rez[a[i]]++;

		return rez;
	}

	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("Provide M");
			System.exit(0);
		}

		int M = Integer.parseInt(args[0]);
		Random rnd = new Random();

		int[] a = new int[rnd.nextInt(1000)];
		for(int i = 0; i < a.length; i++)
			a[i] = rnd.nextInt(M);

		int[] rez = histogram(a, M);

		int max, min;
			max = min = 0;

		for(int i = 1; i < rez.length; i++) {
			if(rez[max] < rez[i])
				max = i;
			else if((rez[min] > rez[i] && rez[i] > 0) || rez[min] == 0)
				min = i;
		}

		System.out.format("Maxim aparitii:\n\t%d a aparut de %d ori\n" +
				"Minim aparitii:\n\t%d a aparut de %d ori\n", max, rez[max], min, rez[min]);
	}
}
