import java.util.Arrays;

public class BinarySearch4 {

	
	public static int rank(int key, int[] a) { 
		int pos = rank(key, a, 0, a.length - 1);
		while(pos > 0 && pos < a.length && a[pos] == a[pos - 1])
			pos--;
		return pos;
	}

	public static int rank(int key, int[] a, int lo, int hi) { 
		// Index of key in a[], if present, is not smaller than lo
		if (lo > hi) return lo;
		int mid = lo + (hi - lo) / 2;
		if(key < a[mid]) return rank(key, a, lo, mid - 1);
		else if (key > a[mid]) return rank(key, a, mid + 1, hi);
		else return mid;
	}

	public static int count(int key, int[] a) {	
		int count = 0;
		int pos = rank(key, a, 0, a.length - 1);
		if(pos < a.length && a[pos] == key) {
			count++;

			int i = pos;
			while( i > 0 && a[--i] == key) {
				count++;
			}

			i = pos;			
			while(i < a.length-1 && a[++i] == key) {
				count++;
			}
		}
		return count;
	}

    public static void main(String[] args) {
        int[] whitelist = In.readInts(args[0]);

        Arrays.sort(whitelist);
		System.out.println(Arrays.toString(whitelist));
        // read key; print if not in whitelist
        while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
			StdOut.println(key + " " + rank(key, whitelist) + " " + count(key, whitelist));
        }
    }
}
