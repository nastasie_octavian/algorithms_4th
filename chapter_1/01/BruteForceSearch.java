import java.util.Arrays;

public class BruteForceSearch {

	public static int rank(int key, int[] a) { 
		for(int i = 0; i < a.length; i++)
			if(a[i] == key)
				return i;
		return -1;
	}
    
	public static void main(String[] args) {
        int[] whitelist = In.readInts(args[0]);

        Arrays.sort(whitelist);

        // read key; print if not in whitelist
        while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            if (rank(key, whitelist) == -1)
                StdOut.println(key);
        }
    }
}
