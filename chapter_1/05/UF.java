import java.lang.reflect.*;

abstract public class UF {
	protected int[] id;
	protected int count;
	protected int accesed;

	public UF(int N) { 
		count = N;
		id = new int[N];
		for(int i = 0; i < N; i++)
			id[i] = i;
	}

	public boolean connected(int p, int q) {
		return find(p) == find(q);
	}

	public int count() {
		return count;
	}	
	
	abstract public int find(int p);
	abstract public void union(int p, int q);

	public int accesed() {
		return accesed;
	}

	public static void main(String[] args) {
		int N = StdIn.readInt();
		try {
			// da, sunt fancy cu reflection, dar mi-e lene sa scriu cod in
			// care doar schimb numele clasei testate, asa ca mai bine
			// scriu cod generic si destept ;)
			Class cls = Class.forName(args[0]);
			UF uf = (UF)cls.getConstructor(int.class).newInstance(N);
			while(!StdIn.isEmpty()) {
				int p = StdIn.readInt();
				int q = StdIn.readInt();
				if(uf.connected(p, q)) continue;
				uf.union(p, q);
				StdOut.println(p + " " + q + " acessed: " + uf.accesed());
			}
			StdOut.println(uf.count() + " components");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
