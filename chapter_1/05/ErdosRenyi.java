/* Exercise 17
*/

public class ErdosRenyi {
	public static int count(int N, UF uf) {
		int count = 0;
		do {
			int p = StdRandom.uniform(N);
			int q = StdRandom.uniform(N);
			count++;
			if(!uf.connected(p, q))
				uf.union(p, q);
		} while(uf.count() > 1);
		return count;
	}
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		UF[] ufs = new UF[] { new QF(N) , new QU(N) , new WQU(N), new WQUPC(N) };
		for(UF uf : ufs)
			StdOut.println(uf.getClass().getName() + " " + count(N, uf));
	}
}
