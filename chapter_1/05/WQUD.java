import java.util.ArrayList;

public class WQUD extends QU {
	protected ArrayList<Integer> sz = new ArrayList<Integer>();
	protected ArrayList<Integer> id = new ArrayList<Integer>();
	
	public WQUD(int N) {
		this();
	}

	public WQUD() {
		super(0);
	}

	public int newSite() {
		id.add(id.size());
		sz.add(1);
		count++;
		return id.size() - 1;
	}
	
	public int find(int p) {
		if(p < id.size()) {
			while(p != id.get(p)) {
				p = id.get(p);
				accesed += 2;
			}
			accesed++;
		} else {
			while(p >= id.size()) {
				newSite();
			}
		}
		return p;
	}

	public void union(int p, int q) {
		accesed = 0;
		int i = find(p);
		int j = find(q);
		if(i == j) return;

		if(sz.get(i) < sz.get(j)) { 
			id.set(i, j); sz.set(j, sz.get(j) + sz.get(i));
		} else {
			id.set(j, i); sz.set(i, sz.get(i) + sz.get(j));
		}
		accesed += 4;
		count--;
	}
}
