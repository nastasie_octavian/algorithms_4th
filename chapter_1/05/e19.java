/* Exercise 18 && 19, 
*/

import java.util.*;

public class e19 {

	public static class Connection {
		public final int p;
		public final int q;

		public Connection(int p, int q) {
			this.p = p;
			this.q = q;
		}
	}

	public static Connection[] generate(int N) {
		List<e19.Connection> cons = new ArrayList<e19.Connection>();
		for(int i = 0; i < N; i++)
			for(int j = 0; j < N; j++)
				if(i != j)
					cons.add(new e19.Connection(i, j));

		Collections.shuffle(cons);
		return cons.toArray(new e19.Connection[0]);
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		try {
			Class cls = Class.forName(args[1]);
			UF uf = (UF)cls.getConstructor(int.class).newInstance(N*N);
			StdDraw.setXscale(0, N-1);
			StdDraw.setYscale(0, N-1);
			StdDraw.setPenRadius(0.008);
			StdDraw.setPenColor(StdDraw.BLUE);
			for(int i = 0; i < N; i++)
				for(int j = 0; j < N; j++)
					StdDraw.point(i, j);
			
			StdDraw.setPenRadius(0.002);
			StdDraw.setPenColor(StdDraw.GRAY);
			Connection[] cons = generate(N*N);
			for(Connection con : cons) {
				
				if(!uf.connected(con.p, con.q)) {
					uf.union(con.p, con.q);
				}					
				int x0 = con.p % N, y0 = con.p / N, 
					x1 = con.q % N, y1=  con.q / N;
				if((x0 == x1 && Math.abs(y0 - y1) == 1) ||
				   (y0 == y1 && Math.abs(x0 - x1) == 1))
				   StdDraw.line(x0, y0, x1, y1);

			} 
			StdOut.println("DONE.");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
