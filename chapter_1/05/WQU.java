public class WQU extends QU {
	protected int[] sz; 
	
	public WQU(int N) {
		super(N);
		sz = new int[N];
		for(int i = 0; i < N; i++)
			sz[i] = 1;
	}

	public void union(int p, int q) {
		accesed = 0;
		int i = find(p);
		int j = find(q);
		if(i == j) return;

		if(sz[i] < sz[j]) { 
			id[i] = j; sz[j] += sz[i];
		} else {
			id[j] = i; sz[i] += sz[j];
		}
		accesed += 4;
		count--;
	}
}
