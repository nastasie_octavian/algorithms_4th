import java.lang.reflect.*;
import java.util.*;

public class e22 {
	
	public static double test(UF uf, e19.Connection[] cons) {
		Stopwatch s = new Stopwatch();
		int k = 0;
		while(uf.count() > 1) {
			if(!uf.connected(cons[k].p, cons[k].q))
				uf.union(cons[k].p, cons[k].q);
			k++;
		}
		return s.elapsedTime();
	}

	public static void main(String[] args) {
		int T = Integer.parseInt(args[0]);
		try {
				Class cls1 = Class.forName(args[1]);
				Class cls2 = Class.forName(args[2]);
				for(int N = 250; true; N *= 2) {
					double time1 = 0, time2 = 0;
					for(int i = 0; i < T; i++) {					
						UF uf1 = (UF)cls1.getConstructor(int.class).newInstance(N);
						UF uf2 = (UF)cls2.getConstructor(int.class).newInstance(N);
						e19.Connection[] cons = e19.generate(N);
						time1 += test(uf1, cons);
						time2 += test(uf2, cons);
					}					
					StdOut.printf("%7s %7.4f ", N, time1 / T);
					StdOut.printf("%7s %7.4f\n", N, time2 / T);
				}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
