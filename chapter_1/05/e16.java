/* Foarte dragut :P, la QF nu prea da graficul cum e in carte
*/

import java.util.ArrayList;

public class e16 {
	public static void main(String[] args) {
		int N = StdIn.readInt();
		ArrayList<Integer> costs = new ArrayList<Integer>();
		try {
			Class cls = Class.forName(args[0]);
			UF uf = (UF)cls.getConstructor(int.class).newInstance(N);
			while(!StdIn.isEmpty()) {
				int p = StdIn.readInt();
				int q = StdIn.readInt();
				if(uf.connected(p, q)) {
					costs.add(uf.accesed());
					continue;
				}
				uf.union(p, q);
				costs.add(uf.accesed());
			}
			StdOut.println(uf.count() + " components");
		} catch(Exception e) {
			e.printStackTrace();
		}
		int max = costs.get(0);
		int min = max;
		for(int i = 1; i < costs.size(); i++) {
			int aux = costs.get(i);
			if(aux > max)
				max = aux;
			else if(aux < min)
				min = aux;
		}
		StdDraw.setXscale(0, costs.size()+1);
		StdDraw.setYscale(0, max + 1);
		int total = 0;
		for(int i = 0; i < costs.size(); i++) {
			total += costs.get(i);
			StdDraw.setPenColor(StdDraw.GRAY);
			StdDraw.point(i, costs.get(i));			
			StdDraw.setPenColor(StdDraw.RED);
			StdDraw.point(i, (double)total / (i + 1));
		}
	}
}
