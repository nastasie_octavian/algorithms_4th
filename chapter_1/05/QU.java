public class QU extends UF {

	public QU(int N) {
		super(N);
	}

	public int find(int p) {
		while(p != id[p]) {
			p = id[p];
			accesed += 2;
		}
		accesed++;
		return p;
	}

	public void union(int p, int q) {
		accesed = 0;
		int pRoot = find(p);
		int qRoot = find(q);
		if(pRoot == qRoot) return;

		id[pRoot] = qRoot;
		accesed++;
		count--;
	}
}
