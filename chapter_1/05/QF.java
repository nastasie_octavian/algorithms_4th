public class QF extends UF {
	
	public QF(int N)  {
		super(N);
	}

	public int find(int p) {
		accesed++;
		return id[p];
	}

	public void union(int p , int q) {
		accesed = 0;
		int pID = find(p);
		int qID = find(q);

		if(pID == qID) return;

		for(int i = 0; i < id.length; i++) {
			accesed ++;
			if(id[i] == pID) {
				id[i] = qID;
				accesed++;
			}
		}
		count--;
	}
}
