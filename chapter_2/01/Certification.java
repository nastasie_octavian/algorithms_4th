import java.util.Arrays;

class Dummy implements Comparable<Dummy> {
	int val;
	int poz;

	public Dummy(int v, int p) {
		val = v;
		poz = p;
	}

	public int compareTo(Dummy o) {
		if(val > o.val)
			return 1;
		if(val < o.val)
			return -1;
		return 0;
	}
}

public class Certification {
	
	public static boolean check(Comparable[] a, String alg) {
		Comparable[] b = new Comparable[a.length];
		for(int i = 0; i < a.length; i++)
			b[i] = a[i];
		Arrays.sort(b);

		if(alg.equals("Insertion")) Insertion.sort(a);
		if(alg.equals("Selection")) Selection.sort(a);
		if(alg.equals("Shell")) 	Shell.sort(a);
		if(alg.equals("Merge")) 	Merge.sort(a);
		if(alg.equals("Quick")) 	Quick.sort(a);
		if(alg.equals("Heap")) 		Heap.sort(a);

		for(int i = 0; i < a.length; i++)
			if(a[i] != b[i])
				return false;
		return true;
	}

	public static void main(String[] args) {
		String alg = args[0];
		int N = Integer.parseInt(args[1]);

		Dummy[] a = new Dummy[N];
		for(int i = 0; i < N; i++)
			a[i] = new Dummy(StdRandom.uniform(N), i);

		if(check(a, alg))
			StdOut.println("OK ... ");
		else
			StdOut.println("Error ... ");
	}
}
