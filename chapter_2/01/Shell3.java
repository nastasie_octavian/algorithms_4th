public class Shell3 {
	
	private static long compares;
	private static long size;

	public static double test() {
		return (double)compares/ size;
	}

	public static void sort(Comparable[] a) {
		int N = a.length;
		size = N;
		compares = 0;
		int h = 1;

		while(h < N/3 ) h = 3*h + 1;

		while(h >= 1) {
			for(int i = h; i < N; i++) {
				for(int j = i; j >= h && less(a[j], a[j-h]); j-=h) {
					compares++;
					exch(a, j, j-h);
				}
				compares++;
			}
			h = h / 3;
		}
	}

	@SuppressWarnings("unchecked")
	private static boolean less(Comparable v, Comparable w) {
		return v.compareTo(w) < 0;
	}

	private static void exch(Comparable[] a, int i, int j) {
		Comparable t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

	private static void show(Comparable[] a) {
		for(int i = 0; i < a.length; i++)
			StdOut.print(a[i] + " ");
		StdOut.println();
	}

	public static boolean isSorted(Comparable[] a) {
		for(int i = 1; i < a.length; i++)
			if(less(a[i], a[i-1])) return false;
		return true;
	}

	public static void main(String[] args) {
		for(int n = 100; true; n *= 10) {
			Double[] a = new Double[n];
			for(int i = 0; i < n; i++)
				a[i] = StdRandom.uniform();

			sort(a);
			StdOut.printf("%10d %9.3f\n", n, test());
		}
	}
}
