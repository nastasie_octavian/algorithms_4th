/* Exercise 11 */
public class Shell2 {
	
	public static void sort(Comparable[] a) {
		int N = a.length;
		
		int[] hs = new int[ (int)(Math.log(N) / Math.log(3)) + 2];
		int k = 0;
		hs[0] = 1;
		while( hs[k] < N/3) 
			hs[++k] = 3* hs[k-1] + 1;

		for(; k > -1; k--) {
			for(int i = hs[k]; i < N; i++) {
				for(int j = i; j >= hs[k] && less(a[j], a[j-hs[k]]); j-=hs[k]) 
					exch(a, j, j-hs[k]);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static boolean less(Comparable v, Comparable w) {
		return v.compareTo(w) < 0;
	}

	private static void exch(Comparable[] a, int i, int j) {
		Comparable t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

	private static void show(Comparable[] a) {
		for(int i = 0; i < a.length; i++)
			StdOut.print(a[i] + " ");
		StdOut.println();
	}

	public static boolean isSorted(Comparable[] a) {
		for(int i = 1; i < a.length; i++)
			if(less(a[i], a[i-1])) return false;
		return true;
	}

	public static void main(String[] args) {
		String[] a = In.readStrings();
		sort(a);
		assert isSorted(a);
		show(a);
	}
}
