import java.awt.Color;

public class e31 {
	public static void main(String[] args) {
		int T = Integer.parseInt(args[0]);
		int Nmax = 16000;
		if(args.length > 1)
			Nmax = Integer.parseInt(args[1]);

		String[] sorts = { "Insertion", "Insertion3", "Insertion4", 
						   "Selection", "Shell" };
		Color[] colors = { Color.darkGray, Color.gray, Color.lightGray, 
							Color.red, Color.black };
		double[][] time = 
			new double[(int)(Math.log(Nmax / 1000)/ Math.log(2)) + 1][sorts.length];
		StdOut.printf("%10s ", "N");
		for(String s : sorts)
			StdOut.printf("%10s ", s);
		StdOut.println();

		for(int i = 0; i < 80; i++)
			StdOut.printf("-");
		StdOut.println();
		
		int k = 0;
		for(int N = 1000; N <= Nmax ; N *= 2) {
			for(int i = 0; i < time.length; i++)
				time[k][i] = 0.0;
			StdOut.printf("%10d ", N);

			for(int j = 0; j < T; j++) {
				Double[] a = new Double[N];
				for(int i = 0; i < N; i++)
					a[i] = StdRandom.uniform();

				for(int i = 0; i < sorts.length; i++) {
					Double[] b = new Double[N];
					System.arraycopy(a, 0, b, 0, a.length);
					time[k][i] += SortCompare.time(sorts[i], b);
				}
			}

			for(double d : time[k])
				StdOut.printf("%10.5f ", d / T);
			StdOut.println();
			k++;

		}

		StdDraw.setXscale(1000, Nmax);
		double max = time[0][0];
		for(double[] row : time) {
			for(double val : row)
				if(val > max)
					max = val;
		}

		max /= T;
		StdDraw.setYscale(0, max);
		StdDraw.setPenRadius(0.005);
		for(int i = 1; i < time.length; i++ ){
			for(int j = 0; j < sorts.length; j++) {
				StdDraw.setPenColor(colors[j]);
				StdDraw.line(Math.pow(2, i-1) * 1000, time[i-1][j]/ T, 
					Math.pow(2, i)*1000, time[i][j] / T);
				}
		}
	}
}
