public class Insertion5 {
	
	public static void sort(double[] a) {
		int N = a.length;
		for(int i = 0; i < N; i++) {
			for(int j = i; j > 0 && less(a[j], a[j-1]); j--) 
				exch(a, j, j-1);
		}
	}

	@SuppressWarnings("unchecked")
	private static boolean less(double v, double w) {
		return v < w;
	}

	private static void exch(double[] a, int i, int j) {
		double t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

	private static void show(double[] a) {
		for(int i = 0; i < a.length; i++)
			StdOut.print(a[i] + " ");
		StdOut.println();
	}

	public static boolean isSorted(double[] a) {
		for(int i = 1; i < a.length; i++)
			if(less(a[i], a[i-1])) return false;
		return true;
	}

	public static void main(String[] args) {
		double[] a = In.readDoubles();
		sort(a);
		assert isSorted(a);
		show(a);
	}
}
