public class Insertion2 {
	
	public static void sort(Double[] a) {
		int N = a.length;
		for(int i = 0; i < N; i++) {
			int j = i;
			for(; j > 0 && less(a[j], a[j-1]); j--) 
				exch(a, j, j-1);
			show(a, i, j);
			try {
				Thread.sleep(1000);
			} catch(Exception e) {
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static boolean less(Comparable v, Comparable w) {
		return v.compareTo(w) < 0;
	}

	private static void exch(Comparable[] a, int i, int j) {
		Comparable t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

	private static void show(Double[] a, int i, int j) {
		StdDraw.setXscale(0, a.length+1);
		StdDraw.clear();
		StdDraw.setPenRadius(0.01);
		StdDraw.setPenColor(StdDraw.GRAY);
		for(int l = 0; l < j; l++)
			StdDraw.line(l, 0, l, a[l]);		
			
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.line(j, 0, j, a[j]);

		StdDraw.setPenColor(StdDraw.BLACK);
		for(int l = j+1; l <= i; l++)
			StdDraw.line(l, 0, l, a[l]);		
			
		StdDraw.setPenColor(StdDraw.GRAY);
		for(int l = i+1; l < a.length; l++)
			StdDraw.line(l, 0, l, a[l]);		


	}

	public static boolean isSorted(Comparable[] a) {
		for(int i = 1; i < a.length; i++)
			if(less(a[i], a[i-1])) return false;
		return true;
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		Double[] a = new Double[N];
		for(int i = 0; i < N; i++)
			a[i] = StdRandom.uniform();

		sort(a);
		assert isSorted(a);
	}
}
