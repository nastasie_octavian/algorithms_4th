public class Selection2 {
	
	public static void sort(Double[] a) {
		int N = a.length;
		for(int i = 0; i < N; i++) {
			int min = i;
			for(int j = i + 1; j < N; j++)
				if(less(a[j], a[min])) min = j;
			exch(a, i, min);
			show(a, i);
			try {
				Thread.sleep(1000);
			} catch(Exception e) {
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static boolean less(Comparable v, Comparable w) {
		return v.compareTo(w) < 0;
	}

	private static void exch(Comparable[] a, int i, int j) {
		Comparable t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

	private static void show(Double[] a, int k) {
		StdDraw.clear();
		StdDraw.setXscale(0, a.length + 1);
		StdDraw.setPenRadius(0.01);
		StdDraw.setPenColor(StdDraw.GRAY);
		for(int i = 0; i < k; i++) {
			StdDraw.line(i, 0, i, a[i]);
		}
		
		StdDraw.setPenColor(StdDraw.RED);
		StdDraw.line(k, 0, k, a[k]);

		StdDraw.setPenColor(StdDraw.BLACK);
		for(int i = k+1; i < a.length; i++)	{
			StdDraw.line(i, 0, i, a[i]);
		}
	}

	public static boolean isSorted(Comparable[] a) {
		for(int i = 1; i < a.length; i++)
			if(less(a[i], a[i-1])) return false;
		return true;
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		Double[] a = new Double[N];
		for(int i = 0; i < N; i++)
			a[i] = StdRandom.uniform();
		sort(a);
		assert isSorted(a);
	}
}
