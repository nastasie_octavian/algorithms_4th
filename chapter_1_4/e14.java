import java.util.Arrays;

public class e14 {

	private static int rankHi(int key, int[] a) {
		int lo = 0;
        int hi = a.length - 1;
		int pos = -1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
			if(a[mid] == key) {
				pos = mid;
			}
            if(key < a[mid]) hi = mid - 1;
            else if(key >= a[mid]) lo = mid + 1;
        }
        return pos;
	}

	public static int count(int[] a) {
		int N = a.length;
		int cnt = 0;
		for(int i = 0; i < N; i++)
			for(int j = i+1; j < N; j++)
				for(int k = j+1; k < N; k++)
					for(int l = k+1; l < N; l++)
						if(a[i] + a[j] + a[k] + a[l] == 0) {
							cnt++;
						}
		return cnt;
	}
	
	public static int fastCount(int[] a) {		
		int N = a.length;
		int cnt = 0;
		for(int i = 0; i < N; i++)
			for(int j = i+1; j < N; j++)
				for(int k = j+1; k < N; k++) {
					int aux = rankHi((-a[i] - a[j] - a[k]), a);
					if(aux > k) {
						cnt++;
						while(aux > k + 1 && a[aux] == a[aux-1]) {
								cnt++;
								aux++;
							}
						}
					}
		return cnt;
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		Arrays.sort(a); // N*log(N)

		Stopwatch s = new Stopwatch();
		int count = count(a);
		double time = s.elapsedTime();
		StdOut.printf("     count: %7d %7.2f\n", count, time); 
		s = new Stopwatch();
		count = fastCount(a);
		time = s.elapsedTime();
		StdOut.printf("fast count: %7d %7.2f\n", count, time);

	}
}
