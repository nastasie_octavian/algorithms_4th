// exercise 3
public class DoublingTest2 {
	public static double timeTrial(int N) {
		int MAX = 1000000;
		int[] a = new int[N];
		for(int i = 0; i < N; i++)
			a[i] = StdRandom.uniform(-MAX, MAX);
		Stopwatch timer = new Stopwatch();
		int cnt = ThreeSumFast.count(a);
		return timer.elapsedTime();
	}

	public static void main(String[] args) {
		double[][] points = new double[20][2];
		points[0] = new double[] { 0.0, 0.0 };
		int ind = 1;
		for(int N = 250; true; N += N) {
			double time =  timeTrial(N);
			StdOut.printf("%7d %5.2f %7.3f\n", N, time, Math.log(Math.log(time)));
			points[ind++] = new double[] { N, Math.log(Math.log(time)) };
			StdDraw.clear();			
			StdDraw.setXscale(0.0, points[ind-1][0]);
			StdDraw.setYscale(0.0, points[ind-1][1]);
			StdDraw.setPenRadius(0.01);
			StdDraw.setPenColor(StdDraw.RED);
			for(int i = 0; i < ind; i++) 
				StdDraw.point(points[i][0], points[i][1]);
			StdDraw.setPenRadius(0.005);
			StdDraw.setPenColor(StdDraw.BLUE);
			for(int i = 0; i < ind - 1; i++)
				StdDraw.line(points[i][0], points[i][1],points[i+1][0], points[i+1][1]);
		}
	}
}
