/*
	Test nou algoritm TwoSum
*/

public class TwoSumTest {
	public static final int MAX = 10000;
	public static void test(int N) {
		int[] a = new int[N];
		for(int i = 0; i < N; i++)
			a[i] = StdRandom.uniform(-MAX, MAX);

		Stopwatch s = new Stopwatch();
		int count = TwoSumFast.count(a);
		double time = s.elapsedTime();
		StdOut.printf("%7d %7.3f ", count, time);		
		
		s = new Stopwatch();
		count = TwoSumFast2.count(a);
		time = s.elapsedTime();
		StdOut.printf("%7d %7.3f\n", count, time);
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		test(N);
	}
}
