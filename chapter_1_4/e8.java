/*
	Exercitiul 8, consider "number of pairs of values ... that are equal" ca 
	toate combinatiile de 2 luate cate nr de valori egale.
*/

import java.util.Arrays;
	
public class e8 {
	
	public static int count(int[] a) {
		Arrays.sort(a); // Nlog(N) mergesort
		int cnt = 0;
		int N = a.length;
		for(int i = 0; i < N-1; i++)
			if(a[i] == a[i+1]) {
				int aux = i;
				while(a[i] == a[i+1])
					i++;
				aux = i - aux + 1;
				cnt += (aux-1)*aux / 2; // Comb de 2 luate cate aux = aux!/2!*(aux-2)!
			}
		return cnt;
	}

	public static void main(String[] args) {
		StdOut.println(count(In.readInts(args[0])));
	}
}
