/*
	Test algoritm TwoSumFast2 pe ThreeSumFast
*/

import java.util.Arrays;

public class ThreeSumFast3 {
	public static int count(int[] a) {
		Arrays.sort(a);
		int N = a.length;
		int cnt = 0;
		for(int i = 0; i < N; i++)
			for(int j = i + 1; j < N; j++){
				int aux = BinarySearch.rank(-a[i]-a[j], a);
				if(aux > j) {
					cnt++;
					N = aux;
				}
			}
		return cnt;
	}

	public static void main(String[] args) {
		int[] a = In.readInts(args[0]);
		StdOut.println(count(a));
	}
}
