public class e6a {
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int sum = 0;
		for(int n = N; n > 0; n /= 2)
			for(int i = 0; i < n; i++)
				sum++;
		StdOut.println("Sum: " + sum);
	}
}
