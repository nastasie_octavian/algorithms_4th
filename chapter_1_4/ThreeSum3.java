// exercise 2, mitigate int overflow
public class ThreeSum3 {
	public static int count(int[] a) {
		int N = a.length;
		int cnt = 0;

		for(int i = 0; i < N; i++)
			for(int j = i +1; j < N; j++)
				for(int k = j + 1; k < N; k++) {
					int rez;
					if( (a[i] >= 0 && Integer.MAX_VALUE - a[i] < a[j]) ||
						(a[i] < 0 && Integer.MIN_VALUE - a[i] > a[j])) {
						continue;
					}
					rez = a[i] + a[j];
					if( (rez >= 0 && Integer.MAX_VALUE - rez < a[k]) ||
						(rez < 0 && Integer.MIN_VALUE - rez < a[k])) {
						continue;
					}
					if(rez + a[k] == 0)
						cnt++;
					}
		return cnt;
	}

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int[] a = new int[N];
		for(int i = 0; i < N; i++)
			a[i] = StdRandom.uniform(-1000000000, 1000000000);
		Stopwatch timer = new Stopwatch();
		int cnt = count(a);
		double time = timer.elapsedTime();
		StdOut.println(cnt + " triples " + time);
	}
}
