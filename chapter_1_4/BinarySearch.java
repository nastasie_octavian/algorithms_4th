/*
	Exercise 10
*/

import java.util.Arrays;

public class BinarySearch {

	public static int rank(int key, int[] a) { 
		return rank(key, a, 0, a.length - 1);
	}

	public static int rank(int key, int[] a, int lo, int hi) { 
		int aux = -1;
		if (lo > hi) return -1;
		int mid = lo + (hi - lo) / 2;
		if(key <= a[mid]) aux = rank(key, a, lo, mid - 1);
		else if (key > a[mid]) return rank(key, a, mid + 1, hi);

		if(aux == -1)
			if(key == a[mid])
				return mid;
			else
				return -1;
		else
			return aux;
	}

    public static void main(String[] args) {
        int[] ints = In.readInts(args[0]);

        Arrays.sort(ints);

        while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            StdOut.println(rank(key, ints));
        }
    }
}
