public class e6c {
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		int sum = 0;
		for(int i = 1; i < N; i *= 2)
			for(int j = 0; j < N; j++)
				sum++;
		StdOut.println("Sum: " + sum);
	}
}
