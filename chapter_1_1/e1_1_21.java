import java.util.*;

public class e1_1_21 {
	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Integer> valA = new ArrayList<Integer>();
		ArrayList<Integer> valB = new ArrayList<Integer>();

		while(!StdIn.isEmpty()) {
			names.add(StdIn.readString());
			valA.add(StdIn.readInt());
			valB.add(StdIn.readInt());
		}

		int size = names.size();
		System.out.format("%10s | %5s %5s %6s\n", "Nume", "ValA", "ValB", "Ratio");
		System.out.format("-------------------------------\n");
		for(int i =0; i < size; i++) {
			System.out.format("%10s | %5d %5d %6.3f\n", names.get(i), valA.get(i), 
				valB.get(i), (double)valA.get(i) / valB.get(i));
		}
	}
}
