/*************************************************************************
 *  Compilation:  javac BinarySearch.java
 *  Execution:    java BinarySearch whitelist.txt < input.txt
 *  Data files:   http://algs4.cs.princeton.edu/11model/tinyW.txt
 *                http://algs4.cs.princeton.edu/11model/tinyT.txt
 *                http://algs4.cs.princeton.edu/11model/largeW.txt
 *                http://algs4.cs.princeton.edu/11model/largeT.txt
 *
 *  % java BinarySearch tinyW.txt < tinyT.txt
 *  50
 *  99
 *  13
 *
 *  % java BinarySearch largeW.txt < largeT.txt | more
 *  499569
 *  984875
 *  295754
 *  207807
 *  140925
 *  161828
 *  [3,675,966 total values]
 *  
 *************************************************************************/

import java.util.Arrays;

public class BinarySearch5 {

	public static int rank(int key, int[] a) { 
		return rank(key, a, 0, a.length - 1);
	}

	public static int rank(int key, int[] a, int lo, int hi) { 
		// Index of key in a[], if present, is not smaller than lo
		if (lo > hi) return -1;
		int mid = lo + (hi - lo) / 2;
		if(key < a[mid]) return rank(key, a, lo, mid - 1);
		else if (key > a[mid]) return rank(key, a, mid + 1, hi);
		else return mid;
	}

	public static int[] generate(int N) {
		int[] a = new int[N];
		for(int i = 0; i < N; i++)
			a[i] = StdRandom.uniform(100000, 1000000);
		Arrays.sort(a);
		return a;
	}

    public static void main(String[] args) {
		int T = Integer.parseInt(args[0]);
		int[] N = { 1000, 10000, 100000, 1000000 };
		int[] avg = new int[N.length];
		for(int i = 0; i < T; i++) {
			for(int k = 0; k < N.length; k++) {
				int[] a = generate(N[k]);
				int[] b = generate(N[k]);

				for(int j = 0; j < N[k]; j++) {
					if(rank(a[j], b) != -1) {
						avg[k]++;
						while(j < N[k] -1 && a[j] == a[j+1])
							j++;
					}
				}
			}
		}

		System.out.format("          |");
		for(int n : N)
			System.out.format("%10d ", n);
		System.out.println();
		for(int i = 0; i <= N.length; i++)
			System.out.print("-----------");
		System.out.println();
		System.out.print("Average   |");
		for(int rez : avg)
			System.out.format("%10.3f ", (double)rez / T);
		System.out.println();
    }
}
