import java.util.Arrays;

public class Matrix {

	public static double dot(double[] x, double[] y) {
		double result = 0;
		int len = x.length < y.length ? x.length : y.length;
		for(int i = 0; i < len; i++) {
			result += x[i] * y[i];
		}
		return result; 
	}

	public static double[][] mult(double[][] a, double[][] b) {
		double[][] result = new double[a.length][b[0].length];
		for(int i = 0; i < result.length; i++)
			for(int j = 0; j < result[0].length; j++)
				for(int k = 0; k < a.length; k++)
					result[i][j] += a[i][k] * b[k][j];
		return result;
	}

	static double[][] transpose(double[][] a) {
		double[][] result = new double[a[0].length][a.length];
		for(int i = 0; i < a.length; i++)
			for(int j = 0; j < a[0].length; j++)
				result[j][i] = a[i][j];
		return result;
	}

	static double[] mult(double[][] a, double[] x) {
		double[] result = new double[a[0].length < x.length ? a[0].length : x.length ];
		for(int i = 0; i < a[0].length && i < x.length; i++)
			result[i] = a[0][i] * x[i];
		return result;
	}

	static double[] mult(double[] y, double[][] a) {
		double[] result = new double[y.length < a[0].length ? y.length : a[0].length];
		for(int i = 0; i < y.length; i++)
			for(int j = 0; j < a.length; j++)
				result[i] += y[j] * a[j][i];
		return result;
	}

	public static void main(String[] args) {
		int N1 = StdIn.readInt();
		int M1 = StdIn.readInt();
		double[][] a = new double[N1][M1];
		for(int i = 0; i < N1; i++)
			for(int j = 0; j < M1; j++)
				a[i][j] = StdIn.readDouble();
				
		int N2 = StdIn.readInt();
		int M2 = StdIn.readInt();
		double[][] b = new double[N2][M2];
		for(int i = 0; i < N2; i++)
			for(int j = 0; j < M2; j++)
				b[i][j] = StdIn.readDouble();

		int N3 = StdIn.readInt();
		double[] x = new double[N3];
		for(int i = 0; i < N3; i++)
			x[i] = StdIn.readDouble();

		int N4 = StdIn.readInt();
		double[] y = new double[N4];		
		for(int i = 0; i < N4; i++)
			y[i] = StdIn.readDouble();

		StdOut.println("Vector dot product: " + dot(x, y));
		StdOut.println(" AxB");
		StdOut.println(Arrays.deepToString(mult(a, b)));
		StdOut.println("A transpose");
		StdOut.println(Arrays.deepToString(transpose(a)));
		StdOut.println("AxX");
		StdOut.println(Arrays.toString(mult(a, x)));		
		StdOut.println("YxA");
		StdOut.println(Arrays.toString(mult(y, a)));
	}
}
