/*************************************************************************
 *  Compilation:  javac BinarySearch.java
 *  Execution:    java BinarySearch whitelist.txt < input.txt
 *  Data files:   http://algs4.cs.princeton.edu/11model/tinyW.txt
 *                http://algs4.cs.princeton.edu/11model/tinyT.txt
 *                http://algs4.cs.princeton.edu/11model/largeW.txt
 *                http://algs4.cs.princeton.edu/11model/largeT.txt
 *
 *  % java BinarySearch tinyW.txt < tinyT.txt
 *  50
 *  99
 *  13
 *
 *  % java BinarySearch largeW.txt < largeT.txt | more
 *  499569
 *  984875
 *  295754
 *  207807
 *  140925
 *  161828
 *  [3,675,966 total values]
 *  
 *************************************************************************/

import java.util.Arrays;

public class BinarySearch3 {

	public static int rank(int key, int[] a) { 
		return rank(key, a, 0, a.length - 1);
	}

	public static int rank(int key, int[] a, int lo, int hi) { 
		// Index of key in a[], if present, is not smaller than lo
		if (lo > hi) return -1;
		int mid = lo + (hi - lo) / 2;
		if(key < a[mid]) return rank(key, a, lo, mid - 1);
		else if (key > a[mid]) return rank(key, a, mid + 1, hi);
		else return mid;
	}

    public static void main(String[] args) {
        int[] whitelist = In.readInts(args[0]);

        Arrays.sort(whitelist);
		int length = whitelist.length, i = 0;
		System.out.println(Arrays.toString(whitelist));
		while(i < length-1) {
			if(whitelist[i] == whitelist[i+1]) {
				for(int j = i+1; j < length - 1; j++) {
					whitelist[j] = whitelist[j+1];
				}
				length--;
			}
			else
				i++;
		}
		int[] aux = new int[length];
		for(i = 0; i < length; i++)
			aux[i] = whitelist[i];
		whitelist = aux;
		System.out.println(Arrays.toString(whitelist));
        // read key; print if not in whitelist
        while (!StdIn.isEmpty()) {
            int key = StdIn.readInt();
            if (rank(key, whitelist) == -1)
                StdOut.println(key);
        }
    }
}
