public class e1_1_9 {
	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("Please provide a number!");
			System.exit(0);
		}
		
		String s = "";
		for(int n = Integer.parseInt(args[0]); n > 0; n /= 2)
			s = (n % 2) + s;
		System.out.println(s);
	}
}
