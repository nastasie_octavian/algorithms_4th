public class e1_1_5 {
	public static void main(String[] args) {
		if(args.length < 2)
			throw new RuntimeException("Too few arguments");
		double x = Double.parseDouble(args[0]);
		double y = Double.parseDouble(args[1]);
		if( 0 < x && x < 1 && 0 < y && y < 1)
			System.out.println("true");
		else
			System.out.println("false");
	}
}
