public class e1_1_20 {
	
	public static double ln(int n) {
		if(n == 1) return Math.log(1);
		return ln(n-1) + Math.log(n);
	}

	public static double fact(int n) {
		if(n == 0) return 1;
		return n * fact(n-1);
	}

	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("Provide input number");
			System.exit(0);
		}

		int n = Integer.parseInt(args[0]);
		double dev = 0;
		for(int i = 1; i < n; i++)
			dev += Math.abs(ln(i) - Math.log(fact(i)));
		System.out.println("Average eps: " + (dev / n));
	}
}
