import java.util.*;

public class e1_1_31 {

	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);
		double p = Double.parseDouble(args[1]);

		double x0 = 0.5, y0 = 0.5, r = 0.5;
		StdDraw.circle(x0, y0, r);

		ArrayList<Double[]> points = new ArrayList<Double[]>();		
		for(int i = 0; i < N; i++) {
			points.add(new Double[] {x0 + r*Math.cos(i * 2 * Math.PI / N), 
				y0 + r*Math.sin(i * 2 * Math.PI / N)});
		}

		StdDraw.setPenColor(StdDraw.BLUE);
		StdDraw.setPenRadius(0.05);
		for(Double[] point : points) {
			StdDraw.point(point[0], point[1]);
		}

		StdDraw.setPenColor(StdDraw.GRAY);
		StdDraw.setPenRadius(0.005);
		for(int i = 0; i < points.size(); i++)
			for(int j = 0; j < points.size(); j++)
				if( i != j && Math.random() > p) {
						Double[] a = points.get(i);
						Double[] b = points.get(j);
						StdDraw.line(a[0], a[1], b[0], b[1]);
					}
	}
}
