import java.util.*;

public class e1_1_13 {
	public static void main(String[] args) {
		if(args.length < 2) {
			System.out.println("M and N required!");
			System.exit(0);
		}
		int M = Integer.parseInt(args[0]);
		int N = Integer.parseInt(args[1]);
		int[][] a = new int[M][N];
		Random rnd = new Random(46);
		for(int i =0; i < M; i++)
			for(int j = 0; j < N; j++)
				a[i][j] = rnd.nextInt(100);

		System.out.println("Original matrix");
		for(int i = 0; i < M; i++) {
			for(int j = 0; j < N; j++)
				System.out.format("%3d ", a[i][j]);
			System.out.println();
		}		


		System.out.println("Transpose matrix");
		for(int i = 0; i < N; i++) {
			for(int j = 0; j < M; j++)
				System.out.format("%3d ", a[j][i]);
			System.out.println();
		}		
	}
}
